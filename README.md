#  correr laravel

php artisan serve 

# crear un modelo con su migracion,seeder , factory ,controlador y recursos
## en laravel los modelos deben empezzar con Mayuscula y estar en singular

php artisan make:model Participante -msfcr

# hacer correr las migraciones
php artisan migrate

# hacer correr las migraciones borrando todo
php artisan migrate:fresh 
# hacer correr los seeders
## todos los seeders
php artisan db:seed 
## un solo seeders
php artisan db:seed --class=PersonaSeeder

# hacer correr las migraciones borrando todo y hace correr los seeder
php artisan migrate:fresh --seed

# mostrar todas las rutas disponibles
php artisan route:list



