<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Ciudad;

class CiudadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Ciudad::create([
            'id' => 1,
            'nombre' => 'Sucre'
        ]);
        Ciudad::create([
            'id' => 2,
            'nombre' => 'La Paz'
        ]);
        Ciudad::create([
            'id' => 3,
            'nombre' => 'Cochabamba'
        ]);
        Ciudad::create([
            'id' => 4,
            'nombre' => 'Oruro'
        ]);
        Ciudad::create([
            'id' => 5,
            'nombre' => 'Potosi'
        ]);
        Ciudad::create([
            'id' => 6,
            'nombre' => 'Tarija'
        ]);
        Ciudad::create([
            'id' => 7,
            'nombre' => 'Santa Cruz'
        ]);
        Ciudad::create([
            'id' => 8,
            'nombre' => 'Trinidad'
        ]);
        Ciudad::create([
            'id' => 9,
            'nombre' => 'Cobija'
        ]);
    }
}
