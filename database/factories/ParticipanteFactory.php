<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Participante>
 */
class ParticipanteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $valor=rand(1,2);
        if ($valor==1)
        {
            $nombre=fake()->firstNameFemale();
            $sexo='F';
        }
        else
        {
            $nombre=fake()->firstNameMale();
            $sexo='M';
        }
        return [
            'carnet_identidad' => fake()->unique()->numberBetween(10000000, 99999999),
            'nombres'=>$nombre,
            'apellidos'=>fake()->lastName(),
            'sexo'=>$sexo,
            'direccion'=>fake()->address(),
            'correo_electronico'=>fake()->unique()->safeEmail(),
            'celular'=>fake()->phoneNumber(),
            'ciudad_id'=>fake()->numberBetween(1,9)
            
        ];
    }
}
