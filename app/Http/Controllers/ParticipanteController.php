<?php
namespace App\Http\Controllers;
use App\Models\Participante;
use Illuminate\Http\Request;
use App\Models\Ciudad;
class ParticipanteController extends Controller
{
    public function index()
    {
        $participantes = Participante::all();
        return view('participantes.index', [
            'participantes' => $participantes
        ]);
    }
    public function create()
    {
        $ciudades = Ciudad::all();
        return view('participantes.create', [
            'ciudades' => $ciudades
        ]);
    }
    public function store(Request $request)
    {
        Participante::create($request->all());
        return redirect()->route('participantes.index');
    }
    public function show(Participante $participante)
    {
        //
    }
    public function edit(Participante $participante)
    {
        $ciudades = Ciudad::all();
        return view('participantes.edit', [
            'participante' => $participante,
            'ciudades' => $ciudades
        ]);
    }
    public function update(Request $request, Participante $participante)
    {
        $participante->update($request->all());
        return redirect()->route('participantes.index');
    }
    public function destroy(Participante $participante)
    {
        $participante->delete();
        return redirect()->route('participantes.index');
    }
}
