<?php

use Illuminate\Support\Facades\Route;
use  App\Http\Controllers\ParticipanteController;
use App\Http\Controllers\CiudadController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Ruta para mostrar la lista de participantes en el index
Route::resource('participantes', ParticipanteController::class);

Route::resource('ciudades', CiudadController::class);


