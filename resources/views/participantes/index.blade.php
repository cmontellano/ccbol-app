@extends('layouts.app')

@section('content')

    <table class="min-w-full divide-y divide-gray-200 dark:divide-gray-700">
        <tr>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Sexo</th>
            <th>Direccion</th>
            <th>Correo Electronico</th>
            <th>Celular</th>
            <th>Ciudad</th>
            <th>Operaciones</th>

        </tr>
        @foreach ($participantes as $participante)
        <tr>
            <td>{{$participante->nombres}}</td>
            <td>{{$participante->apellidos}}</td>
            <td>{{$participante->sexo}}</td>
            <td>{{$participante->direccion}}</td>
            <td>{{$participante->correo_electronico}}</td>
            <td>{{$participante->celular}}</td>
            <td>{{$participante->ciudad->nombre}}</td>
            <td>
                <a class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" href="{{route('participantes.edit',$participante->id)}}">Editar</a>
                <form action="{{route('participantes.destroy',$participante->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <input class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" type="submit" value="Eliminar">
                </form>
            </td>
        </tr>
        @endforeach   

    </table>

    <a href="{{route('participantes.create')}}">Insertar Participante</a>
    @endsection