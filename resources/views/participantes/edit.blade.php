@extends('layouts.app')

@section('content')

<form class="w-full max-w-lg" action="{{route('participantes.update',$participante->id)}}" method="post">
    @csrf
    @method('PUT')
    <label class="block uppercase tracking-wide text-red-500 text-xs font-bold mb-5"  for="nombres">Carnet Identidad</label>
    <input class="appearance-none block w-full lg:1/2 bg-gray-200 text-gray-700 border  rounded py-2 px-3  leading-tight focus:outline-none focus:bg-white" id="grid-first-name" type="text" name="carnet_identidad" value="{{$participante->carnet_identidad}}">
    <label for="nombres">Nombres</label>
    <input class="appearance-none block w-full lg:1/2 bg-gray-200 text-gray-700 border  rounded py-2 px-3  leading-tight focus:outline-none focus:bg-white" type="text" name="nombres" value="{{$participante->nombres}}">
    <label for="apellidos">Apellidos</label>
    <input class="appearance-none block w-full lg:1/2 bg-gray-200 text-gray-700 border  rounded py-2 px-3  leading-tight focus:outline-none focus:bg-white" type="text" name="apellidos" value="{{$participante->apellidos}}">
    <br>
    <label for="sexo">Sexo</label>

    <input type="radio" name="sexo" value="M" @checked($participante->sexo=='M')>M
    <input type="radio" name="sexo" value="F" @checked($participante->sexo=='F')>F
    <br>
    <label for="direccion">Direccion</label>
    <input class="appearance-none block w-full lg:1/2 bg-gray-200 text-gray-700 border  rounded py-2 px-3 leading-tight focus:outline-none focus:bg-white" type="text" name="direccion" value="{{$participante->direccion}}">
    <br>
    <label for="correo_electronico">Correo Electronico</label>
    <input class="appearance-none block w-full lg:1/2 bg-gray-200 text-gray-700 border  rounded py-2 px-3  leading-tight focus:outline-none focus:bg-white" type="email" name="correo_electronico" value="{{$participante->correo_electronico}}">
    <br>
    <label for="celular">Celular</label>
    <input class="appearance-none block w-full lg:1/2 bg-gray-200 text-gray-700 border  rounded py-2 px-3 leading-tight focus:outline-none focus:bg-white" type="text" name="celular" value="{{$participante->celular}}">
    <br>
    <label for="ciudad">Ciudad</label>
    <select
    class="relative z-20 w-full appearance-none rounded border border-stroke bg-transparent px-5 py-3 outline-none transition " name="ciudad_id">

        @foreach ($ciudades as $ciudad)
        <option value="{{$ciudad->id}}" @selected($ciudad->id==$participante->ciudad_id) >{{$ciudad->nombre}}
        </option>
        @endforeach
    </select>
    <br>
    <input class="py-2 px-2 rounded transition ease-in-out delay-150 bg-blue-500 hover:-translate-y-1 hover:scale-110 hover:bg-indigo-500 duration-300" type="submit" value="Actualizar">
   






</form>
@endsection