@extends('layouts.app')

@section('content')

    <form action="{{route('participantes.store')}}" method="post">
        @csrf
        <label for="nombres">Carnet Identidad</label>
        <input type="text" name="carnet_identidad">
        <br>
        <label for="nombres">Nombres</label>
        <input type="text" name="nombres">
        <br>
        <label for="apellidos">Apellidos</label>
        <input type="text" name="apellidos">
        <br>
        <label for="sexo">Sexo</label>
        <input type="radio" name="sexo" value="M">M
        <input type="radio" name="sexo" value="F">F
        <br>
        <label for="direccion">Direccion</label>
        <input type="text" name="direccion">
        <br>
        <label for="correo_electronico">Correo Electronico</label>
        <input type="email" name="correo_electronico">
        <br>
        <label for="celular">Celular</label>
        <input type="text" name="celular">
        <br>
        <label for="ciudad">Ciudad</label>
        <select name="ciudad_id">
            @foreach ($ciudades as $ciudad)
            <option value="{{$ciudad->id}}">{{$ciudad->nombre}}</option>
            @endforeach
        </select>
        <br>
        <input type="submit" value="Registrar">






    </form>

    @endsection